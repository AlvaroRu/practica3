package com.example.practica3

import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.practica3.ui.TimePickerFragment
import kotlinx.android.synthetic.main.activity_picker.*

class PickerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker)
    }

    fun showDatePickerDialog(v: View) {
        //val timePickerFragment = TimePickerFragment()
        val timerPickerFragment = TimePickerFragment.noInstance(TimePickerDialog.OnTimeSetListener
        { view, hourOfDay, minute ->
            pkrTIme.setText("$(hourOfDay):$(minute)")
        })
        timePickerFragment.show(supportFragmentManager, "datePicker")
    }
}